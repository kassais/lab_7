package lab.android.khai.lab_7

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit private var button: Button

    @SuppressLint("WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button = findViewById<Button>(R.id.logicButton)
        button.setOnClickListener {
            val node = findViewById<TextView>(R.id.maxDiffResult)
           val res = max_diff(findViewById<EditText>(R.id.maxDiffText).getText()!!.toString())
            node.text = "\n $res"
        }

        button = findViewById<Button>(R.id.iterationButton)
        button.setOnClickListener {
            val node = findViewById<TextView>(R.id.iterationResult)
            val res = disp_digits(findViewById<EditText>(R.id.iterationText).getText()!!.toString())
            node.text = "\n $res"
        }

        button = findViewById<Button>(R.id.arrayButton)
        button.setOnClickListener {
            val node = findViewById<TextView>(R.id.arrayResult)
            val res = arrayOddEven(findViewById<EditText>(R.id.arrayText).getText()!!.toString())
            node.text = "\n $res"
        }
    }

    fun max_diff(str: String): String{

        var numbers = mutableListOf<Double>()
        try{
            numbers =  str.split(',').map { it!!.toDouble() }.toMutableList()
        }catch (e: Throwable){
            toastOnError()
            return ""
        }

       val max = numbers.max()

        numbers.remove(max)

        return (numbers.sum() / max!!.toDouble()).toString()

    }

    fun disp_digits(N: String): String {

        var i = 0
        var n = 0
        var foundFlag = false
        var result = ""
        try {
            n = N.toInt()
        } catch (e: Throwable) {
            toastOnError()
            return result
        }

        do {

            var divider = Math.pow(10.toDouble(), i.toDouble())
            var digited = (n / divider).toDouble()

            if (digited >= 1 && digited < 10) {

                for (iter in i downTo 0) {

                    var power = Math.pow(10.toDouble(), iter.toDouble())

                    var forOutput = n - n % power

                    result += "${(forOutput / power).toInt()},"

                    n -= forOutput.toInt()
                }

                foundFlag = true
            }

            i++
        } while (!foundFlag)

        return result

    }

    fun arrayOddEven(str: String): String{

        var A = mutableListOf<Double>()
        try{
             A =  str.split(',').map { it!!.toDouble() }.toMutableList()
        }catch (e: Throwable){
            toastOnError()
            return ""
        }

        fun callback(index: Int): Int {

            when {
                A[index]%2 === 0.0  -> return 1
                else -> return -1
            }

        }

        var iter = 0
        var changeFlag = true

        do {
            if (callback(iter) + callback(iter + 1) == 0) {
                iter++
            } else {
                changeFlag = false
            }


        } while (iter < (A.size - 1) && changeFlag)


       if(changeFlag) return "0" else return "1"

    }

    fun toastOnError(){

        Toast.makeText(applicationContext, "Неправильный ввод!", Toast.LENGTH_SHORT).show()
    }

}
